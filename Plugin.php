<?php namespace Mja\Store;

use System\Classes\PluginBase;
use Backend;

/**
 * store Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'mja.store::lang.name',
            'description' => 'mja.store::lang.description',
            'author'      => 'Matiss Janis Aboltins',
            'icon'        => 'icon-cart-plus',
            'homepage'    => 'http://mja.lv/',
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Mja\Store\Components\CategoryList' => 'categoryList',
            'Mja\Store\Components\ProductList' => 'productList',
            'Mja\Store\Components\SingleProduct' => 'singleProduct',
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'store' => [
                'label'       => 'mja.store::lang.navigation.base',
                'url'         => Backend::url('mja/store/products'),
                'icon'        => 'icon-cart-plus',
                'permissions' => ['mja.store.*'],
                'order'       => 100,

                'sideMenu' => [
                    'products' => [
                        'label'       => 'mja.store::lang.navigation.products',
                        'icon'        => 'icon-cubes',
                        'url'         => Backend::url('mja/store/products'),
                        'permissions' => ['mja.store.access_products']
                    ],
                    'categories' => [
                        'label'       => 'mja.store::lang.navigation.categories',
                        'icon'        => 'icon-tags',
                        'url'         => Backend::url('mja/store/categories'),
                        'permissions' => ['mja.store.access_categories']
                    ]
                ]
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'mja.store.access_products' => [
                'label' => 'mja.store::lang.permissions.access_products',
                'tab' => 'mja.store::lang.name',
            ],
            'mja.store.access_categories' => [
                'label' => 'mja.store::lang.permissions.access_categories',
                'tab' => 'mja.store::lang.name',
            ],
            'mja.store.products_import' => [
                'label' => 'mja.store::lang.permissions.products_import',
                'tab' => 'mja.store::lang.name',
            ],
            'mja.store.access_settings' => [
                'label' => 'mja.store::lang.permissions.access_settings',
                'tab' => 'mja.store::lang.name',
            ],
            'mja.store.products_export' => [
                'label' => 'mja.store::lang.permissions.products_export',
                'tab' => 'mja.store::lang.name',
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'mja.store::lang.settings.label',
                'description' => 'mja.store::lang.settings.description',
                'category'    => 'mja.store::lang.settings.category',
                'icon'        => 'icon-cog',
                'class'       => 'Mja\Store\Models\Settings',
                'order'       => 100,
                'keywords'    => 'store ecommerce shop cart money',
                'permissions' => ['mja.store.access_settings']
            ]
        ];
    }

}
