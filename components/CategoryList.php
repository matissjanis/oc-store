<?php namespace Mja\Store\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Mja\Store\Models\Category;

class CategoryList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'mja.store::lang.component.categoryList.name',
            'description' => 'mja.store::lang.component.categoryList.description',
        ];
    }

    public function defineProperties()
    {
        return [
            'categoryPage' => [
                'title'             => 'mja.store::lang.component.categoryList.property_categoryPage',
                'description'       => 'mja.store::lang.component.categoryList.property_categoryPage_desc',
                'default'           => 'store/index',
                'type'              => 'dropdown',
                'showExternalParam' => false,
            ],
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Grab all of the categories.
     *
     * Can be accessed with {{ __SELF__.categories }}.
     *
     * @return Collection
     */
    public function categories()
    {
        return Category::all()->toNested();
    }

    /**
     * Get the URL of a category.
     * @param  Category $category
     * @return string
     */
    public function categoryPage(Category $category)
    {
        $slug = $category->getParentsAndSelf()->implode('slug', '/');
        return Page::url($this->property('categoryPage'), ['slug' => $slug]);
    }

}
