<?php namespace Mja\Store\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Mja\Store\Models\Product;

class ProductList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'mja.store::lang.component.productList.name',
            'description' => 'mja.store::lang.component.productList.description',
        ];
    }

    public function defineProperties()
    {
        return [
            'categorySlug' => [
                'title'             => 'mja.store::lang.component.productList.property_categorySlug',
                'description'       => 'mja.store::lang.component.productList.property_categorySlug_desc',
                'default'           => '{{ :slug }}',
                'type'              => 'string',
                'group'             => 'mja.store::lang.component.productList.group_pagination',
            ],
            'itemsPerPage' => [
                'title'             => 'mja.store::lang.component.productList.property_itemsPerPage',
                'description'       => 'mja.store::lang.component.productList.property_itemsPerPage_desc',
                'default'           => 20,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items property can contain only numeric symbols',
                'group'             => 'mja.store::lang.component.productList.group_pagination',
                'showExternalParam' => false,
            ],
            'page' => [
                'title'             => 'mja.store::lang.component.productList.property_page',
                'description'       => 'mja.store::lang.component.productList.property_page_desc',
                'default'           => 1,
                'group'             => 'mja.store::lang.component.productList.group_pagination',
                'type'              => 'string',
            ],

            'productListPage' => [
                'title'             => 'mja.store::lang.component.productList.property_productListPage',
                'description'       => 'mja.store::lang.component.productList.property_productListPage_desc',
                'default'           => 'store/index',
                'type'              => 'dropdown',
                'group'             => 'mja.store::lang.component.productList.group_pages',
                'showExternalParam' => false,
            ],

            'productPage' => [
                'title'             => 'mja.store::lang.component.productList.property_productPage',
                'description'       => 'mja.store::lang.component.productList.property_productPage_desc',
                'default'           => 'store/product',
                'type'              => 'dropdown',
                'group'             => 'mja.store::lang.component.productList.group_pages',
                'showExternalParam' => false,
            ],
        ];
    }

    /**
     * Returns a list of all pages.
     * @return array
     */
    public function getProductListPageOptions()
    {
        return $this->getProductPageOptions();
    }

    /**
     * Get a list of all the pages.
     * @return array
     */
    public function getProductPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Get all of the publicly visible products and paginate them.
     *
     * Can be accessed with {{ __SELF__.products }}.
     *
     * @return Collection
     */
    public function products()
    {
        $query = Product::query();

        if ($categorySlugParts = $this->property('categorySlug')) {
            $query->whereHas('category', function ($q) use ($categorySlugParts) {
                $q->whereIn('slug', explode('/', $categorySlugParts));
            });
        }

        return $query->paginate(
            $this->property('itemsPerPage'),
            $this->property('page')
        );
    }

    /**
     * Get the page URl of a certain product.
     * @param  Product $product
     * @return string
     */
    public function productPage(Product $product)
    {
        return Page::url($this->property('productPage'), $product->toArray());
    }

    /**
     * Get the page URL of the product list page.
     * @param  array  $attributes
     * @return string
     */
    public function productListPage(array $attributes = [])
    {
        return Page::url($this->property('productListPage'), $attributes);
    }

}
