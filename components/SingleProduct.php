<?php namespace Mja\Store\Components;

use Cms\Classes\ComponentBase;

class SingleProduct extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'SingleProduct Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

}