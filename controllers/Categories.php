<?php namespace Mja\Store\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mja.Store', 'store', 'categories');

        $this->addJs('/modules/cms/assets/js/october.cmspage.js', 'core');
        $this->addJs('/modules/backend/assets/js/october.tabformexpandcontrols.js', 'core');
    }

    public function listOverrideColumnValue($record, $columnName, $definition = null)
    {
        if ($columnName === 'title') {
            return trim(str_repeat('‣', $record->getDepth()) . ' ' . $record->title);
        }
    }
}
