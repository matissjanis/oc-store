<?php
return [
    'name' => 'Store',
    'description' => 'Open source e-commerce store plugin.',

    'navigation' => [
        'base' => 'Store',
        'products' => 'Products',
        'categories' => 'Categories',
    ],

    'permissions' => [
        'access_products' => 'Access Products',
        'products_import' => 'Import Products',
        'products_export' => 'Export Products',
        'access_categories' => 'Access Categories',
        'access_settings' => 'Access Settings',
    ],

    'settings' => [
        'label' => 'Store settings',
        'description' => 'Manage various store settings',
        'category' => 'Store',

        'currency' => 'Currency',
        'currency_comment' => 'Currency used throughout the Mja.Store plugin.',
    ],

    'component' => [
        'productList' => [
            'name' => 'Product List',
            'description' => 'Displays a list of all the publicly visible products.',

            'group_pages' => 'Pages',
            'group_pagination' => 'Pagination',

            'property_itemsPerPage' => 'Items per page.',
            'property_itemsPerPage_desc' => 'Maximum number of products that will be displayed in a single page.',
            'property_page' => 'Current page.',
            'property_page_desc' => 'The current page number. URL parameters can be used.',
            'property_productPage' => 'Single product page',
            'property_productPage_desc' => 'CMS page used for displaying a single product.',
            'property_productListPage' => 'Product list page',
            'property_productListPage_desc' => 'CMS page used for displaying a list of products.',
        ],

        'categoryList' => [
            'name' => 'Category List',
            'description' => 'Displays a list of all the categories.',

            'property_categoryPage' => 'Category page',
            'property_categoryPage_desc' => 'Category aware page that has a product list component.',
        ],
    ],

    'product' => [
        'id' => 'ID',
        'title' => 'Title',
        'slug' => 'Slug',
        'description' => 'Description',
        'price' => 'Price',
        'category' => 'Category',

        'index' => 'Manage Products',
        'form' => 'Product',
        'reorder' => 'Reorder Products',
        'export' => 'Export Products',
        'import' => 'Import Products',

        'btn_new' => 'New Product',
        'btn_import' => 'Import products',
        'btn_export' => 'Export products',
        'return_btn' => 'Return to product list',
    ],

    'category' => [
        'id' => 'ID',
        'title' => 'Title',
        'slug' => 'Slug',
        'parent_category' => 'Parent category',

        'index' => 'Manage Categories',
        'form' => 'Category',
        'reorder' => 'Reorder Category list',

        'btn_new' => 'New Category',
        'return_btn' => 'Return to category list',
    ],
];
