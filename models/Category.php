<?php namespace Mja\Store\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\NestedTree;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mja_store_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'title' => 'required|between:4,255',
        'slug' => 'required|alpha_dash|between:1,255|unique:mja_store_categories',
        'parent' => 'integer',
    ];

    /**
     * @var array Relations, 'scope' => 'withChildren'
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => ['Mja\Store\Models\Product'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
