<?php namespace Mja\Store\Models;

use Cms\Classes\Page;
use Model;

/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Sortable;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mja_store_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'title' => 'required|between:4,255',
        'slug' => 'required|alpha_dash|between:1,255|unique:mja_store_categories',
        'price' => 'required|numeric|min:0',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category' => ['Mja\Store\Models\Category'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Get price with a currency prefix/postfix.
     *
     * Example: 130.99 EUR
     *
     * @todo Replace currency string with actual symbols.
     * @return string
     */
    public function getFrontendPriceAttribute()
    {
        return number_format($this->price, 2) . ' ' .
            Settings::get('mja.store::currency');
    }
}
