<?php namespace Mja\Store\Models;

use Backend\Models\ExportModel;

class ProductExport extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $products = Product::all();
        $products->each(function ($product) use ($columns) {
            $product->addVisible($columns);
        });

        return $products->toArray();
    }
}
