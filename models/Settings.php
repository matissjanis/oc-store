<?php namespace Mja\Store\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'mja_store_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
